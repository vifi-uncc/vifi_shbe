import pandas as pd
import sys, argparse, yaml, json

#test online commit
# test notifications

def updateScheduleTypeLimitsFraction():
	''' Updates SchedulTypeLimits Fraction section '''

	updates=[]
	updates.append("ScheduleTypeLimits,\n")
	updates.append("    Fraction,                !- Name\n")
	updates.append("    0,                       !- Lower Limit Value\n")
	updates.append("    1,                       !- Upper Limit Value\n")
	updates.append("    Continuous,              !- Numeric Type\n")
	updates.append("    Dimensionless;           !- Unit Type\n")
	return updates

def updateScheduleCompactOfficeLightsSuraj(in_data_file):
	''' Updates Schedule:Compact BLDG_LIGHT_SCH '''

	record = pd.read_csv(in_data_file, skiprows=0, header=None)
	record[0] = pd.to_datetime(record[0])
	record["WeekDay"] = record[0].dt.dayofweek
	record["Date"] = record[0].dt.date
	record["Hour"] = record[0].dt.hour

	weekdays = [i+1 for i in range(7)]
	hours = [i for i in range(24)]

	res = pd.DataFrame(index=weekdays, columns=hours)

	for w in weekdays:
		date = record.loc[(record["WeekDay"] == w-1)]
		for h in hours:
			date_hour = date.loc[(record["Hour"] == h)]
			
			num = date_hour.shape[0]
			occ = float(date_hour.loc[(date_hour[1] == 'Occupied')].shape[0])
			res[h][w] = round(occ/num, 3)
		
	#res.to_csv(in_data_file+".processed.csv")

	week_map = {1:'Monday',
		2:'Tuesday',
		3:'Wednesday',
		4:'Thursday',
		5:'Friday'}

	updates=[]
	updates.append("Schedule:Compact,\n")
	updates.append("    Office-Lights-Suraj,          !- Name\n")
	updates.append("    Fraction,                !- Schedule Type Limits Name\n")
	updates.append("    Through: 31 Dec,         !- Field 1\n")

	for week in range(1,6):
		updates.append("    For: "+week_map[week]+",\n")
			
		for hour in range(24):
			updates.append("    Until: "+str(hour+1)+":00,\n")
			updates.append("    "+str(res[hour][week])+",\n")
				
	updates.append("    For: SummerDesignDay,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    1,\n")
	updates.append("    For: WinterDesignDay,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    0,\n")
	updates.append("    For: Holidays AllOtherDays,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    0.05;\n")
	return updates

def updateScheduleCompactOfficeOccupancySuraj(in_data_file):
	''' Updates Schedule:Compact BLDG_LIGHT_SCH '''

	record = pd.read_csv(in_data_file, skiprows=0, header=None)
	record[0] = pd.to_datetime(record[0])
	record["WeekDay"] = record[0].dt.dayofweek
	record["Date"] = record[0].dt.date
	record["Hour"] = record[0].dt.hour

	updates=[]
	updates.append("Schedule:Compact,\n")
	updates.append("    Office-Occupancy-Suraj,          !- Name\n")
	updates.append("    Fraction,                !- Schedule Type Limits Name\n")
	updates.append("    Through: 31 Dec,         !- Field 1\n")
    
	tempdate=''
	tempOccupancy=''
	for index, row in record.iterrows():
		if(tempdate==''):
			rowDate=str(row["Date"])
			rowDateInFormat=rowDate[5:].replace("-","/")
			updates.append("    For: "+rowDateInFormat+",\n")
			tempOccupancy=row[1]
			tempdate=row["Date"]
		elif(tempdate==row["Date"]):
			if(tempOccupancy!=row[1]):
				if(str(row["Hour"]-1)!='-1'):
					updates.append("    Until: "+str(row["Hour"]-1)+":00,\n")
					if(tempOccupancy=="Occupied"):
						updates.append("    "+"1"+",\n")
					else:
						updates.append("    "+"0"+",\n")
				tempOccupancy=row[1]
		else:
			updates.append("    Until: "+"23"+":00,\n")
			if(tempOccupancy=="Occupied"):
				updates.append("    "+"1"+",\n")
			else:
				updates.append("    "+"0"+",\n")

			rowDate=str(row["Date"])
			rowDateInFormat=rowDate[5:].replace("-","/")
			updates.append("    For: "+rowDateInFormat+",\n")
			if(tempOccupancy!=row[1]):
				if(str(row["Hour"]-1)!='-1'):
					updates.append("    Until: "+str(row["Hour"]-1)+":00,\n")
					if(tempOccupancy=="Occupied"):
						updates.append("    "+"1"+",\n")
					else:
						updates.append("    "+"0"+",\n")
				tempOccupancy=row[1]
			tempdate=row["Date"]
                
	updates.append("    For: SummerDesignDay,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    1,\n")
	updates.append("    For: WinterDesignDay,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    0,\n")
	updates.append("    For: Holidays AllOtherDays,\n")
	updates.append("    Until: 24:00,\n")
	updates.append("    0.05;\n")
	return updates

#if __name__ == "__main__":
    ##Note: CSV file should be in ASCII encoding
    #in_data_file = "I:\\Chrome Downloads\\CMU_Occ_Data.csv"
    #in_data_file = str(sys.argv[1])

########## STEP 0: LOAD REQUIRED DATA FILE, UPDATE PARAMETERS, AND OTHER INITIALS ############
parser = argparse.ArgumentParser()
parser.add_argument("-i","--in_file",required=True, help="Data file to analyze", type=argparse.FileType('r'))
parser.add_argument("-o","--out_file",help="Output file containing required updates from data analysis")
parser.add_argument('-u','--update_param', help="YAML file with required parameters to be analyzed",type=argparse.FileType('r'))        # Load update criterion
parser.add_argument('-j','--JSON', action='store_true', help="If true, then processing results are also written to a JSON file")
arguments = parser.parse_args()

# Load input data to be analyzed
in_data_file=arguments.in_file

# Get name of output file
if arguments.out_file:
	out_file=arguments.out_file
else:
	out_file=in_data_file.name+".schedule.idf"

# Loading YAML file containing sections that need to be processed (updated).
if arguments.update_param:
	update_param = yaml.load(arguments.update_param)
else:
	update_param=None

# Determine if analysis results should also be written to a JSON file
JSONUpdates=arguments.JSON
if JSONUpdates:
	updates_json={}

###### Write updates in IDF structure file AND OPTIONALLY TO JSON FILE ######
with open(out_file, 'w+') as f:
	if update_param:
		for key in update_param:    # Each key has a sequence (i.e., list) of values
			if key.upper()=="SCHEDULETYPELIMITS":
				for val in update_param[key]:
					if val.upper()=="FRACTION":
						updates=updateScheduleTypeLimitsFraction()
						for item in updates:
							f.write(item)
						f.write("\n")
						if JSONUpdates:    # If JSON also required, then update JSON object
							if "ScheduleTypeLimits" not in updates_json.keys():    # If ScheduleTypeLimits does not exist, then make one
								updates_json["ScheduleTypeLimits"]={}
							updates_json["ScheduleTypeLimits"]["Fraction"]=updates
			if key.upper()=="SCHEDULE:COMPACT":
				for val in update_param[key]:
					if val.upper()=="OFFICE-LIGHTS-SURAJ":
						updates=updateScheduleCompactOfficeLightsSuraj(in_data_file.name)
						for item in updates:
							f.write(item)
						f.write("\n")
						if JSONUpdates:    # If JSON also required, then update JSON object
							if "Schedule:Compact" not in updates_json.keys():    # If Schedule:Compact does not exist, then make one
								updates_json["Schedule:Compact"]={}
							updates_json["Schedule:Compact"]["Office-Lights-Suraj"]=updates
					if val.upper()=="OFFICE-OCCUPANCY-SURAJ":
						updates=updateScheduleCompactOfficeOccupancySuraj(in_data_file.name)
						for item in updates:
							f.write(item)
						f.write("\n")
						if JSONUpdates:    # If JSON also required, then update JSON object
							if "Schedule:Compact" not in updates_json.keys():    # If Schedule:Compact does not exist, then make one
								updates_json["Schedule:Compact"]={}
							updates_json["Schedule:Compact"]["Office-Occupancy-Suraj"]=updates
	else:
		print("There are no update paremeters. So, nothing to do")
		print()
###### Write all requested updates in JSON file #####
if JSONUpdates:
	with open(out_file+".json", 'w+') as fjson:
		json.dump(updates_json,fjson)
